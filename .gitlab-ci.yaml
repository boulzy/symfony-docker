stages:
    - build
    - qa
    - test
    - release

workflow:
    rules:
        - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
        - if: $CI_COMMIT_TAG
        - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

.docker:
    image: docker:24
    services:
        - name: docker:dind
    before_script:
        - echo "$CI_REGISTRY_PASSWORD" | docker login -u "$CI_REGISTRY_USER" --password-stdin $CI_REGISTRY

.build:
    stage: build
    extends:
        - .docker

.release:
    stage: release
    extends:
        - .docker

.image:
    image: $CI_REGISTRY_IMAGE/project-name:$CI_COMMIT_REF_SLUG
    variables:
        GIT_STRATEGY: none
    before_script:
        - cd /app

.coverage:
    variables:
        XDEBUG_MODE: coverage
        ARTIFACT: $CI_PROJECT_DIR/coverage
    before_script:
        - mkdir -p $ARTIFACT
        - mkdir -p /app/var/coverage
        - cd /app

build:prod:
    extends:
        - .build
    script:
        - docker build --pull -t $CI_REGISTRY_IMAGE/build:prod --target build .
        - docker push $CI_REGISTRY_IMAGE/build:prod
    rules:
        - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
          changes:
              - Dockerfile
              - .docker/**/*
        - if: $CI_COMMIT_TAG
          changes:
              - Dockerfile
              - .docker/**/*

build:dev:
    extends:
        - .build
    script:
        - docker build --pull -t $CI_REGISTRY_IMAGE/build:dev --target build-dev .
        - docker push $CI_REGISTRY_IMAGE/build:dev
    rules:
        - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
          changes:
              - Dockerfile
              - .docker/**/*
        - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
          changes:
              - Dockerfile
              - .docker/**/*

build:ci:
    stage: build
    extends:
        - .docker
    needs:
        - job: build:dev
          optional: true
    script:
        - docker build --pull --build-arg="DEV_BUILD_IMAGE=$CI_REGISTRY_IMAGE/build:dev" -t $CI_REGISTRY_IMAGE/project-name:$CI_COMMIT_REF_SLUG --target ci .
        - docker push $CI_REGISTRY_IMAGE/project-name:$CI_COMMIT_REF_SLUG
    rules:
        - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
        - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

qa:php-cs-fixer:
    stage: qa
    extends:
        - .image
    script:
        - vendor/bin/php-cs-fixer check --stop-on-violation --no-interaction
    rules:
        - if: $CI_PIPELINE_SOURCE == 'merge_request_event'

qa:phpstan:
    stage: qa
    extends:
        - .image
    script:
        - vendor/bin/phpstan analyze --no-interaction
    rules:
        - if: $CI_PIPELINE_SOURCE == 'merge_request_event'

test:phpunit:
    stage: test
    extends:
        - .image
    script:
        - vendor/bin/phpunit
    rules:
        - if: $CI_PIPELINE_SOURCE == 'merge_request_event'

test:behat:
    stage: test
    extends:
        - .image
    script:
        - vendor/bin/behat
    rules:
        - if: $CI_PIPELINE_SOURCE == 'merge_request_event'

coverage:phpunit:
    stage: test
    extends:
        - .image
        - .coverage
    script:
        - vendor/bin/phpunit --coverage-text --coverage-php $ARTIFACT/phpunit.cov
    artifacts:
        paths:
            - coverage/phpunit.cov
    rules:
        - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

coverage:behat:
    stage: test
    extends:
        - .image
        - .coverage
    script:
        - vendor/bin/behat --tags '~@exclude-ci'
        - mv var/coverage/behat.cov $ARTIFACT/behat.cov
    artifacts:
        paths:
            - coverage/behat.cov
    rules:
        - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

coverage:report:
    stage: test
    extends:
        - .image
        - .coverage
    needs:
        - coverage:phpunit
        - coverage:behat
    script:
        - cd $CI_PROJECT_DIR
        - phpcov merge --text=$ARTIFACT/report.txt --html=$ARTIFACT/html/ coverage
        - cat $ARTIFACT/report.txt
    artifacts:
        paths:
            - coverage/html/
    rules:
        - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    coverage: '/^\s*Lines:\s*\d+.\d+\%/'

release:latest:
    extends:
        - .release
    dependencies: []
    script:
        - docker build --pull --build-arg="PROD_BUILD_IMAGE=$CI_REGISTRY_IMAGE/build:prod" -t $CI_REGISTRY_IMAGE:latest --target prod .
        - docker push $CI_REGISTRY_IMAGE:latest
    rules:
        - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

release:tag:
    extends:
        - .release
    dependencies: []
    script:
        - docker build --pull --build-arg="PROD_BUILD_IMAGE=$CI_REGISTRY_IMAGE/build:prod" -t $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG --target prod .
        - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
    rules:
        - if: $CI_COMMIT_TAG

pages:
    stage: release
    extends:
        - .coverage
    needs:
        - coverage:report
    script:
        - cp -Rf $ARTIFACT/html/* $CI_PROJECT_DIR/public/
    artifacts:
        paths:
            - public
    rules:
        - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
