<?php

namespace App\Fidry\AliceDataFixtures;

use Doctrine\ORM\EntityManagerInterface;
use Fidry\AliceDataFixtures\LoaderInterface;
use Fidry\AliceDataFixtures\Persistence\PurgeMode;
use Fidry\AliceDataFixtures\Persistence\PurgerFactoryInterface;
use Nelmio\Alice\FilesLoaderInterface;
use Nelmio\Alice\Throwable\LoadingThrowable;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Finder\Finder;

/** @codeCoverageIgnore */
class Loader implements LoaderInterface
{
    public function __construct(
        #[Autowire(service: 'nelmio_alice.files_loader.simple')] private FilesLoaderInterface $loader,
        #[Autowire(service: 'fidry_alice_data_fixtures.persistence.doctrine.purger.purger_factory')] private PurgerFactoryInterface $purgerFactory,
        #[Autowire(value: '%kernel.project_dir%/fixtures')] private string $fixturesPath,
        private EntityManagerInterface $em,
    ) {
    }

    /**
     * @param string[] $fixturesFiles
     * @param mixed[]  $parameters
     * @param object[] $objects
     *
     * @return object[]
     *
     * @throws LoadingThrowable
     */
    public function load(array $fixturesFiles = null, array $parameters = [], array $objects = [], PurgeMode $purgeMode = null): array
    {
        if (null === $fixturesFiles) {
            $fixturesFiles = [];
            $finder = new Finder();
            $finder->files()->in($this->fixturesPath)->followLinks()->name(['*.yaml', '*.yml']);
            foreach ($finder as $file) {
                $fixturesFiles[] = $file->getRealPath();
            }
        } else {
            $fixturesFiles = \array_map(
                fn (string $fixtureFile) => \str_starts_with($fixtureFile, '/') ? $fixtureFile : "{$this->fixturesPath}/{$fixtureFile}",
                $fixturesFiles
            );
        }

        $fixtures = $this->loader->loadFiles($fixturesFiles, $parameters, $objects)->getObjects();

        foreach ($fixtures as $fixture) {
            $this->em->persist($fixture);
        }

        if (null === $purgeMode) {
            $purgeMode = PurgeMode::createTruncateMode();
        }

        if ($purgeMode !== PurgeMode::createNoPurgeMode()) {
            $purger = $this->purgerFactory->create($purgeMode);
            $purger->purge();
        }

        $this->em->flush();

        return $fixtures;
    }
}
