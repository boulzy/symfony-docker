<?php

namespace App\Command;

use App\Fidry\AliceDataFixtures\Loader;
use Doctrine\Bundle\DoctrineBundle\Command\CreateDatabaseDoctrineCommand;
use Doctrine\Bundle\DoctrineBundle\Command\DropDatabaseDoctrineCommand;
use Doctrine\Migrations\Tools\Console\Command\MigrateCommand;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

/** @codeCoverageIgnore */
#[AsCommand(
    name: 'app:database:initialize',
    description: 'Initialize the database: drop and/or create the database, run the migrations and load the fixtures.',
    aliases: ['app:database:reset', 'app:database', 'app:db']
)]
class InitializeDatabaseCommand extends Command
{
    private SymfonyStyle $io;

    public function __construct(
        #[Autowire(service: 'doctrine.database_drop_command')] private DropDatabaseDoctrineCommand $dropDatabase,
        #[Autowire(service: 'doctrine.database_create_command')] private CreateDatabaseDoctrineCommand $createDatabase,
        #[Autowire(service: 'doctrine_migrations.migrate_command')] private MigrateCommand $migrate,
        private Loader $loader,
        #[Autowire(param: 'kernel.environment')] private string $env,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addOption('drop', 'd', InputOption::VALUE_NONE, 'Drop the database')
            ->addOption('migrations', 'm', InputOption::VALUE_REQUIRED, 'Run the migrations', true)
            ->addOption('fixtures', 'f', InputOption::VALUE_NONE, 'Load the fixtures')
            ->setHelp(<<<EOF
The <info>%command.name%</info> command initializes the database.

<info>php %command.full_name% [--drop] [--fixtures] [--migrations=<yes|no>]</info>

Use the --drop (-d) option to drop and fully reset the database.
Use the --migrations (-f) option with a boolean-like value to enable/disable migrations (enabled by default).
Use the --fixtures (-f) option to initialize the database with Alice fixtures.

Examples:

Drop and recreate the database:
    <info>php %command.full_name% --drop --migrations=no</info>

Update the database and reload fixtures:
    <info>php %command.full_name% --fixtures</info>

Fully reset and reload the database:
    <info>php %command.full_name% --drop --fixtures</info>
EOF
            )
        ;
    }

    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->io->comment("Initialize database for {$this->env} environment");

        try {
            $drop = \filter_var($input->getOption('drop'), FILTER_VALIDATE_BOOLEAN);
            $migrate = \filter_var($input->getOption('migrations'), FILTER_VALIDATE_BOOLEAN);
            $fixtures = \filter_var($input->getOption('fixtures'), FILTER_VALIDATE_BOOLEAN);

            if ($drop) {
                $this->dropDatabase($output);
            }

            $this->createDatabase($output);

            if ($migrate) {
                $this->runMigrations($output);
            }

            if ($fixtures) {
                $this->loadFixtures();
            }
        } catch (\Exception $e) {
            if ($this->io->isQuiet()) {
                $this->io->getErrorStyle()->writeln($e->getMessage());

                return self::FAILURE;
            }

            $this->io->getErrorStyle()->error($e->getMessage());
            if ($this->io->isVerbose()) {
                $this->io->writeln($e->getTraceAsString());
            }

            return self::FAILURE;
        }

        $this->io->success('Database initialized');

        return self::SUCCESS;
    }

    private function dropDatabase(OutputInterface $output): void
    {
        $dropInput = new ArrayInput([
            '--force' => null,
            '--if-exists' => null,
        ]);
        $dropInput->setInteractive(false);
        $this->dropDatabase->run($dropInput, $output);
    }

    private function createDatabase(OutputInterface $output): void
    {
        $createInput = new ArrayInput([
            '--if-not-exists' => null,
        ]);
        $createInput->setInteractive(false);
        $this->createDatabase->run($createInput, $output);
    }

    private function runMigrations(OutputInterface $output): void
    {
        $migrateInput = new ArrayInput([
            '--all-or-nothing' => null,
            '--allow-no-migration' => null,
        ]);
        $migrateInput->setInteractive(false);
        $this->migrate->run($migrateInput, $output);
    }

    private function loadFixtures(): void
    {
        $this->loader->load();
        $this->io->writeln('<fg=green>Fixtures loaded</>');
    }
}
