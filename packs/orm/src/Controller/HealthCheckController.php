<?php

namespace App\Controller;

use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\SchemaValidator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Annotation\Route;

#[AsController]
#[Route('/healthcheck', name: 'healthcheck')]
class HealthCheckController
{
    public function __invoke(EntityManagerInterface $em): Response
    {
        try {
            $em->getConnection()->connect();

            $schemaValidator = new SchemaValidator($em);
            $schemaIsValidated = empty($schemaValidator->validateMapping());
            $schemaIsSynced = $schemaValidator->schemaInSyncWithMetadata();
            $dbStatus = ($schemaIsValidated && $schemaIsSynced) ? 'ok' : 'nok';

            $em->getConnection()->close();
        } catch (Exception) {
            $dbStatus = 'ko';
        }

        return new JsonResponse([
            'status' => 'ok',
            'database' => $dbStatus,
        ]);
    }
}
