<?php

namespace App\Faker\Provider;

use Faker\Provider\Base;
use Symfony\Component\Uid\Factory\UuidFactory;
use Symfony\Component\Uid\Uuid;

/** @codeCoverageIgnore */
class ValueObjectProvider extends Base
{
    public static function uuid(string $uuid = null): Uuid
    {
        if (null === $uuid) {
            return (new UuidFactory(7))->create();
        }

        return Uuid::fromString($uuid);
    }

    public static function date(string $date = null): \DateTimeImmutable
    {
        if (null === $date) {
            return self::randomDate();
        }

        return new \DateTimeImmutable($date);
    }

    private static function randomDate(string $startDate = '-10 years', string $endDate = '+10 years'): \DateTimeImmutable
    {
        $minTimestamp = \strtotime($startDate);
        $maxTimestamp = \strtotime($endDate);

        if (!$minTimestamp) {
            throw new \RuntimeException("Unable to calculate start date {$startDate}");
        }

        if (!$maxTimestamp) {
            throw new \RuntimeException("Unable to calculate start date {$endDate}");
        }

        $randomTimestamp = \rand($minTimestamp, $maxTimestamp);

        return (new \DateTimeImmutable())->setTimestamp($randomTimestamp);
    }
}
