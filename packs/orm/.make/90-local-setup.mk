##@ [Local Setup]

.PHONY: install
install: ## Install the project
install: make-init check-requirements reset database

.PHONY: check-requirements
check-requirements: ## Check that the minimum requirements are met on your host
	@printf "$(BOLD_BLUE)Checking local requirements...$(NO_COLOR)\n"
	@tools="docker make"; \
	for tool in $$tools; do \
		command -v $$tool >/dev/null 2>&1 && printf "✅ $$tool\n" || { printf "❌ $(BOLD_RED)$$tool$(RED) not found$(NO_COLOR)\n"; exit 1; } \
	done;
	@[ ! "$$(docker ps -a | grep boulzy-traefik-docker)" ] \
		&& printf "❌ $(BOLD_RED)boulzy/traefik-docker$(RED) is not running$(NO_COLOR)\n" \
		|| printf "✅ boulzy/traefik-docker"
	@printf "\n"
