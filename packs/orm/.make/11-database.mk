##@ [Database]

.PHONY: migration
migration: ARGS ?=
migration: ## Create a new migration
	@$(SYMFONY) doctrine:migrations:diff $(ARGS)

.PHONY: migrate
migrate: ARGS ?= --no-interaction --allow-no-migration
migrate: ## Execute the migrations
	@$(SYMFONY) doctrine:migrations:migrate $(ARGS)

.PHONY: database
database: ARGS ?= --drop --fixtures
database: ## Setup a fresh database
	@$(SYMFONY) app:database:initialize $(ARGS)
