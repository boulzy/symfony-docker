<?php

namespace App\Tests\Behat\Listener;

use Behat\Testwork\EventDispatcher\Event\SuiteTested;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class BehatListener implements EventSubscriberInterface
{
    public function beforeSuite(): void
    {
        $process = Process::fromShellCommandline('bin/console app:database:initialize');
        $process->run();

        if (!$process->isSuccessful()) {
            echo $process->getErrorOutput();

            throw new ProcessFailedException($process);
        }

        echo $process->getOutput();
    }

    public static function getSubscribedEvents(): array
    {
        return [
            SuiteTested::BEFORE => 'beforeSuite',
        ];
    }
}
