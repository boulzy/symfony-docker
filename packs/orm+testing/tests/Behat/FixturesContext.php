<?php

namespace App\Tests\Behat;

use App\Fidry\AliceDataFixtures\Loader;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\TableNode;
use Behat\Step\Given;

class FixturesContext implements Context
{
    public function __construct(
        private Loader $loader,
    ) {
    }

    #[Given('/^I load the fixtures$/')]
    #[Given('/^I load the fixtures from:$/')]
    #[Given('/^I load the fixtures from "([^"]*)"$/')]
    public function givenILoadTheFixtures(TableNode|string $param = null): void
    {
        if (null === $param) {
            $this->loader->load();

            return;
        }

        if (\is_string($param)) {
            $fixtures = [$param];
        } else {
            $fixtures = $param->getRowsHash();
        }

        $this->loader->load($fixtures);
    }
}
