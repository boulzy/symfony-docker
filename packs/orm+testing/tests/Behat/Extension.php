<?php

namespace App\Tests\Behat;

use App\Tests\Behat\Listener\BehatListener;
use Behat\Testwork\ServiceContainer\ExtensionManager;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;

class Extension implements \Behat\Testwork\ServiceContainer\Extension
{
    public function getConfigKey(): string
    {
        return 'init_database';
    }

    public function load(ContainerBuilder $container, array $config): void
    {
        $databaseListener = (new Definition(BehatListener::class))
            ->addTag('event_dispatcher.subscriber')
        ;

        $container->setDefinition(BehatListener::class, $databaseListener);
    }

    public function configure(ArrayNodeDefinition $builder): void
    {
    }

    public function initialize(ExtensionManager $extensionManager): void
    {
    }

    public function process(ContainerBuilder $container): void
    {
    }
}
