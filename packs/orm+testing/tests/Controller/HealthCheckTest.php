<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HealthCheckTest extends WebTestCase
{
    private KernelBrowser $client;

    protected function setUp(): void
    {
        $this->client = static::createClient();
    }

    public function test(): void
    {
        $this->client->request('GET', '/healthcheck');

        $this->assertResponseIsSuccessful();
        $this->assertSame(\json_encode(['status' => 'ok', 'database' => 'ok']), $this->client->getResponse()->getContent());
    }
}
