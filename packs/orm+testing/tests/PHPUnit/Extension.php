<?php

namespace App\Tests\PHPUnit;

use PHPUnit\Runner\Extension\Facade;
use PHPUnit\Runner\Extension\ParameterCollection;
use PHPUnit\TextUI\Configuration\Configuration;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class Extension implements \PHPUnit\Runner\Extension\Extension
{
    public function bootstrap(Configuration $configuration, Facade $facade, ParameterCollection $parameters): void
    {
        $process = Process::fromShellCommandline('bin/console app:database:initialize --fixtures');
        $process->run();

        if (!$process->isSuccessful()) {
            echo $process->getErrorOutput();

            throw new ProcessFailedException($process);
        }

        echo $process->getOutput();
    }
}
