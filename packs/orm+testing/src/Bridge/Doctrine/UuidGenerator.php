<?php

namespace App\Bridge\Doctrine;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Id\AbstractIdGenerator;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator as SymfonyUuidGenerator;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Symfony\Component\Uid\Uuid;

class UuidGenerator extends AbstractIdGenerator
{
    private readonly SymfonyUuidGenerator $uuidGenerator;

    public function __construct(
        #[Autowire(service: 'doctrine.uuid_generator')] SymfonyUuidGenerator $uuidGenerator = null,
    ) {
        $this->uuidGenerator = $uuidGenerator ?? new SymfonyUuidGenerator();
    }

    public function generateId(EntityManagerInterface $em, $entity): Uuid
    {
        if (null === $entity) {
            return $this->uuidGenerator->generateId($em, $entity);
        }

        $classMetadata = $em->getClassMetadata(\get_class($entity));

        $propertyAccessor = new PropertyAccessor();
        $id = $propertyAccessor->getValue($entity, $classMetadata->getIdentifier()[0]);

        if (null === $id) {
            return $this->uuidGenerator->generateId($em, $entity);
        }

        return $id;
    }
}
