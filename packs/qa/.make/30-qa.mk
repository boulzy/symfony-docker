##@ [Quality Analysis]

.PHONY: qa
qa: ## Run the QA tools
qa: php-cs-fixer phpstan

.PHONY: php-cs-fixer
php-cs-fixer: ## Run Behat tests
	@$(COMPOSE) exec $(COMPOSE_APP) vendor/bin/php-cs-fixer fix

.PHONY: phpstan
phpstan: ## Run PHPUnit tests
	@$(COMPOSE) exec $(COMPOSE_APP) vendor/bin/phpstan analyze
