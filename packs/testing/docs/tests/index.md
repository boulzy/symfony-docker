# Tests

In this section, you will get familiar with the different types of test used in
this project and how they works.

Here are the expectations about the code:

- The domain behaves as intended, so the application building blocks can be trusted
- The connections to the real world are working
- Everything works well once plugged together

## Unit Tests

Unit Tests are designed to evaluate the smallest units of code in isolation.
The code from the Domain layer is the perfect candidate for them, as it is
also designed to be run in isolation.

Unit Tests will ensure that we have a fast and easy way to test the business logic
and build trust in the application core.

[Learn more about the Unit Tests](00-unit-tests.md)

## Use Case Tests

Use Case Tests ensure that for a given use case, the different types of objects
from the Domain layer interact as intended with each other.

Use Cases are best described with scenarios. This also provide a documentation
of the usage of the application. They should reflect the expectations about the
feature, such as the user story or acceptance criteria.

The Gherkin langage is an interesting choice to write use case scenarios,
as it can be understood both by humans and code. This way, they can be reviewed
by everyone involved in the project, making sure that everyone share the same
comprehension of the domain.

[Learn more about the Use Case Tests](01-use-case-tests.md)

## Integration Tests

Integration Tests are used to check that multiple components work together as
intended. They are useful to test code that integrate with external services.

In a clean architecture application, the connection to external services is done
through Ports & Adapters. Thus, to check that the application correctly integrates
with external dependencies, we need to test the Adapters (Ports implementations).

That's why we'll also call these tests Adapter Tests.

We can distinguish two types of Ports:

- Outgoing Port: that's the ports allowing the application to communicate with
  the real world (database, external API, ...). We want to make sure that Adapters
  respect the Port contracts.

- Incoming Port: that's the ports allowing the external world to communicate with
  the application (ex: HTTP requests). For this type of port, we need to make sure
  that Adapters call the use cases the right way.

### Contract Tests

Contract Tests are used to test the Outgoing Ports. They make sure that Adapters
respect the Port contract.

[Learn more about the Contract Tests](04-contract-tests.md)

### Driving Tests

Driving Tests are used to test the Incoming Ports. They check that Adapters call
the use cases the right way.

[Learn more about the Driving Tests](04-driving-tests.md)

## End to End Tests

End to End Tests validate that the entire application is working in a realistic
environment. They run in a black box, meaning that they don't interact with the
code: they only test the inputs and outputs of the application.

Their purpose is to check that all components of the application function correctly
together.

End-to-End Tests may be done with tools such as [Panther](https://github.com/symfony/panther)
or by a QA team on a dedicated environment.

[Learn more about the End to End Tests](04-functional-tests.md)
