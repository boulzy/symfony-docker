# Use Case Tests

The application use cases are described using scenarios written in Gherkin.
We use Behat to run tests based on these scenarios.

```bash
$ make use-case-tests
# or in the application container:
$ vendor/bin/behat --profile use-case-tests
```

## Isolation

To test our use cases, we don't need to test the interactions with the external
world. Replacing Adapters by stubs or lighter versions will improve the execution
speed of the tests and make sure that failing tests are not caused by an external
dependency.

In this application, "in memory" implementations of repositories interfaces are
used to test our use cases without depending on an external storage system. All
external dependencies should be replaced in a similar way.

To this end, we use a dedicated container that will use these implementations.
Fortunately, Symfony already provides a great way to deal with Container
configuration. Stubs and lighter implementations are defined in the
`/config/services_stubs.yaml`. This file is only loaded when using the
`App\Tests\Kernel`.

`friends-of-behat/symfony-extension` provides an integration between Behat and
Symfony, allowing to use Behat contexts as services. By configuring the extension
to use the `App\Tests\Kernel`, the use cases can be injected into Behat contexts
without relying on external dependencies.

> Replacing every interaction with external dependencies may be a lot of work.
> You may prefer development speed to the execution speed and tests fiability
> provided by doing so. As always, choose what's best for your project and your
> team.

## Write a Use Case Test

First, write the scenarios that describe the tested use case.
Scenarios are located in `/features/use-cases/`.

Each `Given`, `When`, `Then` and `And` line wrote must be implemented in a Behat
context. Use case contexts are located in `/tests/UseCases/Contexts`.
