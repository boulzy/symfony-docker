##@ [Tests]

.PHONY: test
test: ## Run the test suites
test: phpunit behat

.PHONY: behat
behat: ## Run Behat tests
	@$(COMPOSE) exec $(COMPOSE_APP) vendor/bin/behat

.PHONY: phpunit
phpunit: ## Run PHPUnit tests
	@$(COMPOSE) exec -e XDEBUG_MODE=coverage $(COMPOSE_APP) vendor/bin/phpunit
