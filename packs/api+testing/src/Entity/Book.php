<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Bridge\Doctrine\UuidGenerator;
use App\Repository\BookRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Symfony\Component\Uid\Uuid;

#[ApiResource]
#[ORM\Entity(repositoryClass: BookRepository::class)]
class Book
{
    public function __construct(
        #[ORM\Id]
        #[ORM\GeneratedValue(strategy: 'CUSTOM')]
        #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
        #[ORM\Column(type: UuidType::NAME, unique: true)]
        private ?Uuid $id = null,

        #[ORM\Column(length: 255)]
        private ?string $title = null,

        #[ORM\Column]
        private ?\DateTimeImmutable $releasedAt = null,
    ) {
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;

        return $this;
    }

    public function getReleasedAt(): ?\DateTimeImmutable
    {
        return $this->releasedAt;
    }

    public function setReleasedAt(\DateTimeImmutable $releasedAt): static
    {
        $this->releasedAt = $releasedAt;

        return $this;
    }
}
