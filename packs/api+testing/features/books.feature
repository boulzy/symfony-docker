Feature: Books Management
    As a user
    In order to manage books
    I want to use the API

    Background:
        Given I load the fixtures from "books.yaml"

    Scenario: List books
        When I send a GET request to "/books"
        Then the response is successful
        And the response body matches json:
            """
            {
                "hydra:totalItems": 101,
                "hydra:member": "@variableType(array)",
                "hydra:member[0]": {
                    "@id": "/books/018cfa9f-feb3-7229-bc50-3448d7a22095",
                    "@type": "Book",
                    "id": "018cfa9f-feb3-7229-bc50-3448d7a22095",
                    "title": "My Book",
                    "releasedAt": "2024-01-11T23:37:34+01:00"
                }
            }
            """

    Scenario: Get an book
        When I send a GET request to "/books/018cfa9f-feb3-7229-bc50-3448d7a22095"
        Then the response is successful
        And the response body matches json:
            """
            {
                "@id": "/books/018cfa9f-feb3-7229-bc50-3448d7a22095",
                "id": "018cfa9f-feb3-7229-bc50-3448d7a22095",
                "title": "My Book",
                "releasedAt": "2024-01-11T23:37:34+01:00"
            }
            """

    Scenario: Create a book
        When I send a POST request to "/books" with body:
            """
            {
                "title": "Some New Book",
                "releasedAt": "2024-01-14T17:05:00+00:00"
            }
            """
        Then the response is successful
        And the response body matches json:
            """
            {
                "@id": "@variableType(string)",
                "id": "@variableType(string)",
                "title": "Some New Book",
                "releasedAt": "2024-01-14T17:05:00+00:00"
            }
            """

    Scenario: Update a book
        When I send a PUT request to "/books/018cfa9f-feb3-7229-bc50-3448d7a22095" with body:
            """
            {
                "title": "Some Other Title",
                "releasedAt": "2022-12-01T10:00:00+00:00"
            }
            """
        Then the response is successful
        And the response body matches json:
            """
            {
                "@id": "@variableType(string)",
                "id": "@variableType(string)",
                "title": "Some Other Title",
                "releasedAt": "2022-12-01T10:00:00+00:00"
            }
            """

    Scenario: Delete a book
        Given I send a DELETE request to "/books/018cfa9f-feb3-7229-bc50-3448d7a22095"
        And the response is successful
        When I send a GET request to "/books/018cfa9f-feb3-7229-bc50-3448d7a22095"
        Then the response status code is 404
