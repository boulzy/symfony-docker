<?php

namespace App\Tests\Controller;

use Boulzy\ArrayComparator\ArrayComparator;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HealthCheckTest extends WebTestCase
{
    private KernelBrowser $client;

    protected function setUp(): void
    {
        $this->client = static::createClient();
    }

    public function test(): void
    {
        $this->client->request('GET', '/healthcheck');

        $jsonComparator = new ArrayComparator();

        $this->assertResponseIsSuccessful();
        $this->assertTrue($jsonComparator->compare(['status' => 'ok', 'database' => 'ok'], $this->client->getResponse()->getContent()));
    }
}
