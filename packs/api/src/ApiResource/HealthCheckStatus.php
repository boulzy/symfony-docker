<?php

namespace App\ApiResource;

enum HealthCheckStatus: string
{
    case OK = 'ok';

    case NOK = 'nok';

    case KO = 'ko';
}
