<?php

namespace App\ApiResource;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\OpenApi\Model\Operation;
use App\State\HealthCheckProvider;

#[ApiResource(
    operations: [
        new Get(
            uriTemplate: '/healthcheck',
            openapi: new Operation(
                summary: 'Display the API status',
                description: 'Display the status (OK or KO) for the available services',
            ),
        ),
    ],
    provider: HealthCheckProvider::class,
)]
class HealthCheck
{
    public function __construct(
        public HealthCheckStatus $status,
        public HealthCheckStatus $database,
    ) {
    }
}
