<?php

namespace App\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\ApiResource\HealthCheck;
use App\ApiResource\HealthCheckStatus;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\SchemaValidator;

/**
 * @implements ProviderInterface<HealthCheck>
 */
class HealthCheckProvider implements ProviderInterface
{
    public function __construct(
        private EntityManagerInterface $em,
    ) {
    }

    public function provide(Operation $operation, array $uriVariables = [], array $context = []): HealthCheck
    {
        try {
            $this->em->getConnection()->connect();

            $schemaValidator = new SchemaValidator($this->em);
            $schemaIsValidated = empty($schemaValidator->validateMapping());
            $schemaIsSynced = $schemaValidator->schemaInSyncWithMetadata();
            $dbStatus = ($schemaIsValidated && $schemaIsSynced) ? HealthCheckStatus::OK : HealthCheckStatus::NOK;

            $this->em->getConnection()->close();
        } catch (Exception) {
            $dbStatus = HealthCheckStatus::KO;
        }

        return new HealthCheck(HealthCheckStatus::OK, $dbStatus);
    }
}
