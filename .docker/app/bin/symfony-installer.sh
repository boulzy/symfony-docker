#!/bin/bash
set -e

if [ -f composer.json ]; then
    printf "❌\t\033[0;31mA Composer project already exists in the current directory\033[0m\n"
    exit 1
fi

rm -Rf tmp/
XDEBUG_MODE=off composer create-project "symfony/skeleton $SYMFONY_VERSION" tmp --stability="${COMPOSER_STABILITY:-stable}" --prefer-dist --no-interaction

cat .gitignore tmp/.gitignore > tmp/.gitignore.tmp && mv tmp/.gitignore.tmp tmp/.gitignore

cd tmp
cp -Rp . ..
cd -
rm -Rf tmp/

XDEBUG_MODE=off composer require "php:>=$PHP_VERSION"
XDEBUG_MODE=off composer config --json extra.symfony.docker 'true'
XDEBUG_MODE=off composer config --json extra.symfony.allow-contrib 'true'

VENDORS=""
DEV_VENDORS=""

if [[ "$ORM_PACK" == "1" ]] || [[ "$API_PACK" == "1" ]]; then
    yq -i -I4 eval-all '. as $item ireduce ({}; . * $item )' compose.local.yaml .docker/compose/compose.postgres.yaml
    yq -i -I4 eval-all '. as $item ireduce ({}; . * $item )' config/packages/framework.yaml packs/orm/config/packages/framework.yaml
    rm -rf packs/orm/config
    VENDORS+="symfony/orm-pack symfony/uid "
    DEV_VENDORS+="hautelook/alice-bundle symfony/maker-bundle "
fi

if [[ "$API_PACK" == "1" ]]; then
    VENDORS+="api "
fi

if [[ "$QA_PACK" == "1" ]]; then
    DEV_VENDORS+="phpstan/phpstan friendsofphp/php-cs-fixer "

    if [[ "$ORM_PACK" == "1" ]] || [[ "$API_PACK" == "1" ]]; then
        DEV_VENDORS+="phpstan/phpstan-doctrine "
    fi
fi

if [[ "$TESTING_PACK" == "1" ]]; then
    DEV_VENDORS+="phpunit/phpunit behat/behat friends-of-behat/symfony-extension symfony/browser-kit symfony/http-client "

    if [[ "$ORM_PACK" == "1" ]] || [[ "$API_PACK" == "1" ]]; then
        DEV_VENDORS+="dama/doctrine-test-bundle "
    fi

    if [[ "$API_PACK" == "1" ]]; then
        DEV_VENDORS+="boulzy/behat-api-platform-bundle "
    fi
fi

if [[ "$DEV_PACK" == "1" ]]; then
    DEV_VENDORS+="symfony/var-dumper symfony/profiler-pack "
fi

if [[ "$VENDORS" != "" ]]; then
    XDEBUG_MODE=off composer require $VENDORS
fi
if [[ "$DEV_VENDORS" != "" ]]; then
    XDEBUG_MODE=off composer require --dev $DEV_VENDORS
fi

if [[ "$ORM_PACK" == "1" ]] || [[ "$API_PACK" == "1" ]]; then
    yq -i -I4 '.doctrine.dbal.use_savepoints = true' config/packages/doctrine.yaml
    rm compose.override.yaml
    sed -i '/^[[:space:]]*###> doctrine\/doctrine-bundle ###/,/^[[:space:]]*###< doctrine\/doctrine-bundle ###/d' compose.yaml
    sed -i '/^volumes/d' 'compose.yaml'
    cd packs/orm && cp -Rp . ../.. && cd -
fi

if [[ "$API_PACK" == "1" ]]; then
    yq -i -I4 'del(.api_platform.prefix)' config/routes/api_platform.yaml
    rm -rf src/Controller/HealthCHeck.php
    cd packs/api && cp -Rp . ../.. && cd -
fi

if [[ "$TESTING_PACK" == "1" ]]; then
    rm behat.yml.dist
    sed -i 's/behat.yml/behat.yaml/' '.gitignore'
    cd packs/testing && cp -Rp . ../.. && cd -

    if [[ "$ORM_PACK" == "1" ]] || [[ "$API_PACK" == "1" ]]; then
    cd packs/orm+testing && cp -Rp . ../.. && cd -
    fi

    if [[ "$API_PACK" == "1" ]]; then
        cd packs/api+testing && cp -Rp . ../.. && cd -
    fi
fi

if [[ "$QA_PACK" == "1" ]]; then
    cd packs/qa && cp -Rp . ../.. && cd -

    if [[ "$ORM_PACK" == "1" ]] || [[ "$API_PACK" == "1" ]]; then
        cd packs/qa+orm && cp -Rp . ../.. && cd -
    fi
fi

XDEBUG_MODE=off composer config --json extra.symfony.allow-contrib 'false'

if [ -f '.env' ]; then
    sed -i "s|^\(DATABASE_URL=\).*|DATABASE_URL=postgresql://docker:docker@postgres:5432/project-name?serverVersion=16.0\&charset=utf8|" '.env'
fi;

if [ -f 'compose.local.yaml' ]; then
    sed -i '/^[[:space:]]*###> install ###/,/^[[:space:]]*###< install ###/d' compose.local.yaml
fi

rm -rf .docker/compose .docker/app/bin/symfony-installer.sh .make/01-new-project.mk docs/symfony-docker packs LICENSE
mv docs/README.md .
