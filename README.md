# 🐋 boulzy/symfony-docker

Create a new fully functional Symfony application in a few minutes.

-----

## 📄 Table of Contents

- [❓ What is boulzy/symfony-docker](#-what-is-boulzysymfony-docker-)
- [❗️ Technical Requirements](#-technical-requirements)
- [⚙️ Installation](#-installation)
- [🐛 Issues](#-issues)
- [📃 License](#-license)

## ❓ What is boulzy/symfony-docker ?

When setting up a new Symfony project, you will usually go through the same
steps: install a new Symfony skeleton, create a Docker environment for the app
to run in, install and configure additional bundles and libraries, such as
Doctrine and testing tools, etc...

**boulzy/symfony-docker** aims to do all this for you. It provides an installer
script and a Docker environment suited for local development, CI and production.
The installer script can be configured to install a minimalist version of Symfony,
or to install a fully functional API connected to a Postgres database, along
with testing and code quality analysis tools.

Be it for a proof-of-concept, a minimum-viable-product or the base for a
long-term complex project, within a few minutes you'll be able to work on what
really matters: the logic of your application.

## ❗ Technical Requirements

- [curl](https://curl.se/)
- [Docker](https://docs.docker.com/get-started/overview/)
- [make](https://en.wikipedia.org/wiki/Make_(software))

If you're not familiar with Docker, I suggest that you learn more about it before
using this project.

Make is used to ease and automate some common tasks, both for installation and
usage of the Symfony projects.

> ℹ️ It is also recommended to install [boulzy/traefik-docker](https://gitlab.com/boulzy/traefik-docker)
> before using this project, as this will allow you to have domain names and
> HTTPS out of the box.

## ⚙️ Installation

The installation of a project is done in a few steps:

### 1. Download boulzy/symfony-docker

Download the latest release from the
[release page](https://gitlab.com/boulzy/symfony-docker/-/releases) and extract
the project from the archive at a path of your convenience.

```shell
$ curl -L https://gitlab.com/boulzy/symfony-docker/-/archive/1.1.1/symfony-docker-1.1.0.tar.gz \
    | tar -xz -C ~/Workspace; mv ~/Workspace/symfony-docker* ~/Workspace/project-name/;
$ cd ~/Workspace/project-name
```

> ⚠️ The directory name of your project will be used as the value for some
> default configurations. You should use a slug version of your project name
> as the root directory name.

### 2. Define your project requirements

#### Versions

You can use five environment variables to further push the setup of your new
Symfony project:

- `ORM_PACK=1`: Install and configure Doctrine with a Postgres database
- `API_PACK=1`: Install and configure API Platform
- `QA_PACK=1`: Install and configure PHP CS Fixer and PHPStan
- `TESTING_PACK=1`: Install and configure PHPUnit and Behat
- `DEV_PACK=1`: Install Symfony VarDumper and Profiler

```yaml
# compose.local.yaml
services:
    app:
        environment:
            ORM_PACK: 1
            API_PACK: 0
            QA_PACK: 1
            TESTING_PACK: 1
            DEV_PACK: 0
```

You can find more information about the packs [here](./docs/symfony-docker/packs.md).

If you want to install a different version of PHP and/or Symfony, please
refer to [this page](./docs/symfony-docker/versions.md).

### 3. Run the installer

Finally, run the installer:

```shell
$ make new
```

When the installation is done, your containers should be up and running, and
your application available at http://localhost.

> ℹ️ If you're using **boulzy/traefik-docker**, don't forget to update the
> hosts:
> 
> ```shell
> $ make -C ~/Workspace/traefik-docker hosts
> # or, if you created an alias:
> $ hosts
> ```
> 
> The project should now be available at https://www.project-name.local.

Go to the new `README.md` to learn more about the usage of the project.
Update the documentation if necessary.

## 🐛 Issues

If you have a problem using this installer, please report it in the
[issue tracker](https://gitlab.com/boulzy/symfony-docker/-/issues).

## 📃 License

**boulzy/symfony-docker** is available under the [MIT License](LICENSE).
