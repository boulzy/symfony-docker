##@ [Docker]

COMPOSE_FILE = compose.yaml
ifneq ($(wildcard compose.$(ENV).yaml),)
	COMPOSE_FILE := $(COMPOSE_FILE):compose.$(ENV).yaml
endif
ifneq ($(wildcard compose.override.yaml),)
	COMPOSE_FILE := $(COMPOSE_FILE):compose.override.yaml
endif
COMPOSE = COMPOSE_FILE=$(COMPOSE_FILE) docker compose
COMPOSE_APP = app

.PHONY: build
build: ARGS ?=
build: SERVICE ?=
build: ## Build the Docker Compose services images
	@$(COMPOSE) build $(ARGS) $(SERVICE)

.PHONY: up
up: ARGS ?= --detach --remove-orphans
up: ## Start the Docker Compose services
	@$(COMPOSE) up $(ARGS)

.PHONY: stop
stop: ## Stop the Docker Compose services
	@$(COMPOSE) stop

.PHONY: down
down: ARGS ?= --volumes --remove-orphans
down: ## Stop and remove the Docker Compose services
down: stop
	@$(COMPOSE) down $(ARGS)

.PHONY: restart
restart: ## Restart the Docker Compose services
restart: stop up

.PHONY: reset
reset: ## Stop, remove, rebuild and restart the Docker Compose services
reset: down build up

.PHONY: logs
logs: ARGS ?= --tail=0 --follow
logs: ## Show the Docker Compose services logs
	@$(COMPOSE) logs $(ARGS)

exec: SERVICE ?= $(COMPOSE_APP)
exec: ARGS ?=
exec: CMD ?=
exec: ## Execute a command in a Docker Compose service
	@$(COMPOSE) exec $(ARGS) $(SERVICE) $(CMD) \
	|| $(COMPOSE) run --rm $(ARGS) $(SERVICE) $(CMD)

.PHONY: sh
sh: SERVICE ?= $(COMPOSE_APP)
sh: ARGS ?=
sh: ## Open a shell in a Docker Compose service
	@$(MAKE) exec ARGS=$(ARGS) SERVICE=$(SERVICE) CMD=sh
