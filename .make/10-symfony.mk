##@ [Symfony]

# Makefile variables
SYMFONY=$(COMPOSE) exec -e APP_ENV=$(APP_ENV) $(COMPOSE_APP) bin/console

.PHONY: cc
cc: ## Clear Symfony cache
	@$(SYMFONY) cache:clear

.PHONY: composer
composer: ## Install the vendors
	@$(MAKE) exec CMD="composer install"
