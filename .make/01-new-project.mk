##@ [New Project]

.PHONY: new
new: ## Install a new Symfony project
new: make-init
	@$(MAKE) build SERVICE="$(COMPOSE_APP)"
	@[ "$$(docker ps -a | grep boulzy-traefik-docker)" ] && $(MAKE) traefik || true;
	@printf "\n"
	@$(MAKE) exec CMD="./.docker/app/bin/symfony-installer.sh";sync;
ifeq ($(OS),darwin)
	@sed -i '' "s/project-name/$$(basename $$PWD)/g" docs/docker/local.md
	@sed -i '' "s/project-name/$$(basename $$PWD)/g" .env
	@sed -i '' "s/project-name/$$(basename $$PWD)/g" .gitlab-ci.yaml
	@sed -i '' "s/project-name/$$(basename $$PWD)/g" compose.local.yaml
	@sed -i '' "s/project-name/$$(basename $$PWD)/g" compose.override.yaml.dist
	@sed -i '' "s/project-name/$$(basename $$PWD)/g" compose.prod.yaml
	@sed -i '' "s/project-name/$$(basename $$PWD)/g" README.md
else
	@sed -i "s/project-name/$$(basename $$PWD)/g" docs/docker/local.md
	@sed -i "s/project-name/$$(basename $$PWD)/g" .env
	@sed -i "s/project-name/$$(basename $$PWD)/g" .gitlab-ci.yaml
	@sed -i "s/project-name/$$(basename $$PWD)/g" compose.local.yaml.dist
	@sed -i "s/project-name/$$(basename $$PWD)/g" compose.override.yaml
	@sed -i "s/project-name/$$(basename $$PWD)/g" compose.prod.yaml
	@sed -i "s/project-name/$$(basename $$PWD)/g" README.md
endif
	@$(MAKE) install
	@printf "\n\n✅\t$(GREEN)Project installed$(NO_COLOR)\n\n\n"

.PHONY: traefik
traefik: ## Setup compose.local.yaml to use boulzy/traefik-docker
	@docker run --rm -v ./:/workdir mikefarah/yq -i -I4 'del(.services.nginx.ports)' compose.local.yaml
	@docker run --rm -v ./:/workdir mikefarah/yq -i -I4 eval-all '. as $$item ireduce ({}; . * $$item )' compose.local.yaml .docker/compose/compose.traefik.yaml
