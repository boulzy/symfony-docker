# syntax=docker/dockerfile:1.4

ARG PHP_VERSION="8.3"
ARG CONTAINER_PROXY
ARG PROD_BUILD_IMAGE=build
ARG DEV_BUILD_IMAGE=build-dev

FROM ${CONTAINER_PROXY}php:$PHP_VERSION-fpm-alpine AS php
FROM ${CONTAINER_PROXY}mlocati/php-extension-installer:2.5 AS php-extension-installer
FROM ${CONTAINER_PROXY}composer:2.8 AS composer
FROM ${CONTAINER_PROXY}mikefarah/yq:4.44 AS yq

FROM php AS build

COPY --link --from=php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/
RUN set -eux; \
    install-php-extensions \
        apcu \
        intl \
        opcache \
    ;

###> recipes ###
###< recipes ###

WORKDIR /app

ARG USER_NAME=app_user
ENV USER_NAME=$USER_NAME
ARG USER_UID=1000
ARG USER_GID=1000

RUN set -eux; \
    mkdir -p var/cache var/log; \
    addgroup -g $USER_GID $USER_NAME; \
    adduser -D -u $USER_UID -G $USER_NAME -s /bin/sh $USER_NAME; \
    chown -R $USER_UID:$USER_GID .;

COPY --link --from=composer /usr/bin/composer /usr/local/bin/composer
ENV COMPOSER_HOME="/home/$USER_NAME/.composer"
ENV PATH="$PATH:$COMPOSER_HOME/vendor/bin"

COPY .docker/app/config/app.ini $PHP_INI_DIR/conf.d/
COPY --chmod=755 .docker/app/bin/docker-entrypoint.sh /usr/local/bin/docker-entrypoint
ENTRYPOINT ["docker-entrypoint"]
CMD ["php-fpm"]

FROM $PROD_BUILD_IMAGE AS prod

RUN mv $PHP_INI_DIR/php.ini-production $PHP_INI_DIR/php.ini;
COPY .docker/app/config/app.prod.ini $PHP_INI_DIR/conf.d/

USER $USER_NAME

COPY --chown=$USER_NAME composer.json composer.lock symfony.lock ./
RUN set -eux; \
    composer install --no-cache --prefer-dist --no-dev --no-autoloader --no-scripts --no-progress;

COPY --chown=$USER_NAME bin         ./bin/
COPY --chown=$USER_NAME config      ./config/
COPY --chown=$USER_NAME migrations  ./migrations/
COPY --chown=$USER_NAME public      ./public/
COPY --chown=$USER_NAME src         ./src/
COPY --chown=$USER_NAME .env        ./

RUN set -eux; \
	composer dump-autoload --classmap-authoritative --no-dev; \
	composer dump-env prod; \
	composer run-script --no-dev post-install-cmd; \
	sync;

USER root
RUN set -eux; \
    find . -type f -name *.php -exec chmod 544 \{\} \;; \
    find . -type f -not -name *.php -exec chmod 444 \{\} \;; \
    find . -type d -exec chmod 544 \{\} \;; \
    chmod 544 bin/console; \
    chmod -R 744 var; \
    rm -rf /usr/local/bin/composer /home/$USER_NAME/.composer /usr/local/bin/install-php-extensions;
USER $USER_NAME

ENV APP_ENV=prod

FROM $PROD_BUILD_IMAGE AS build-dev

RUN set -eux; \
    apk add --no-cache bash git;

RUN set -eux; install-php-extensions xdebug;
ENV XDEBUG_MODE=off

RUN set -eux; \
    curl -L -o /usr/local/bin/phpcov https://phar.phpunit.de/phpcov.phar; \
    chmod +x /usr/local/bin/phpcov;

FROM $DEV_BUILD_IMAGE AS dev

COPY --link --from=yq /usr/bin/yq /usr/local/bin

RUN mv $PHP_INI_DIR/php.ini-development $PHP_INI_DIR/php.ini;
COPY .docker/app/config/app.dev.ini $PHP_INI_DIR/conf.d/

USER $USER_NAME

FROM $DEV_BUILD_IMAGE AS ci

RUN mv $PHP_INI_DIR/php.ini-production $PHP_INI_DIR/php.ini;
COPY .docker/app/config/app.prod.ini $PHP_INI_DIR/conf.d/

USER $USER_NAME

COPY --chown=$USER_NAME composer.json composer.lock symfony.lock ./
RUN set -eux; \
    composer install --no-cache --prefer-dist --no-autoloader --no-scripts --no-progress;

COPY --chown=$USER_NAME bin         ./bin/
COPY --chown=$USER_NAME config      ./config/
COPY --chown=$USER_NAME features    ./features/
COPY --chown=$USER_NAME fixtures    ./fixtures/
COPY --chown=$USER_NAME migrations  ./migrations/
COPY --chown=$USER_NAME public      ./public/
COPY --chown=$USER_NAME src         ./src/
COPY --chown=$USER_NAME tests       ./tests/
COPY --chown=$USER_NAME \
    .env \
    .php-cs-fixer.dist.php \
    behat.yaml.dist \
    phpstan.dist.neon \
    phpunit.xml.dist \
    ./

RUN set -eux; \
	composer dump-autoload --classmap-authoritative; \
	composer dump-env test; \
	composer run-script post-install-cmd; \
	sync;

USER root
RUN set -eux; \
    find . -type f -name *.php -exec chmod 544 \{\} \;; \
    find . -type f -not -name *.php -exec chmod 444 \{\} \;; \
    find . -type d -exec chmod 544 \{\} \;; \
    chmod 544 bin/console vendor/bin/behat vendor/bin/php-cs-fixer vendor/bin/phpstan vendor/bin/phpunit; \
    chmod -R 744 var; \
    rm -rf /usr/local/bin/composer /home/$USER_NAME/.composer /usr/local/bin/install-php-extensions;
USER $USER_NAME
