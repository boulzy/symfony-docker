SHELL := bash
ifeq ($(shell uname),Windows_NT)
    SHELL := bash.exe
endif

.SHELLFLAGS := -euo pipefail -c
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules

OS?=$(shell uname)
ifeq ($(OS),Linux)
OS=linux
endif
ifeq ($(OS),Darwin)
OS=darwin
endif
ifeq ($(OS),Windows_NT)
OS=windows
endif
OS_ARCH?=$(shell uname -m)

include .make/variables.env
-include .make/.env

ENV ?=
APP_ENV ?=

include .make/helpers/colors.mk
include .make/*.mk

.DEFAULT_GOAL:=help
.PHONY: help
help:
	@printf '  \033[1mDefault values: \033[0m     \n'
	@printf '  ===================================\n'
	@printf '  ENV: \033[31m "$(ENV)" \033[0m     \n'
	@printf '  APP_ENV: \033[31m "$(APP_ENV)" \033[0m     \n'
	@printf '  ===================================\n'
	@printf '  \033[3mRun the following command to set them:\033[0m\n'
	@printf '  \033[1mmake make-init ENVS="ENV=prod"\033[0m\n'
	@printf '  \033[3mWhen running Symfony commands, you can set the\033[0m\n'
	@printf '  \033[3mSymfony environment using the APP_ENV argument.\033[0m\n'
	@printf '  \033[1mmake cc APP_ENV=test\033[0m\n'
	@awk 'BEGIN {FS = ":.*##"; printf "\n\033[1mUsage:\033[0m\n  make \033[36m<target>\033[0m\n"} /^[a-zA-Z0-9_-]+:.*?##/ { printf "  \033[36m%-40s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' Makefile .make/*.mk

ENVS?=ENV=local APP_ENV=dev
.PHONY: make-init
make-init: ## Initializes the local .make/.env file with ENV variables for make (args: make make-init ENVS="ENV=prod")
	@$(if $(ENVS),,$(error ENVS is undefined))
	@rm -f .make/.env
	@for variable in $(ENVS); do \
		echo $$variable | tee -a .make/.env > /dev/null 2>&1; \
	done
	@echo "Created a local .make/.env file"
