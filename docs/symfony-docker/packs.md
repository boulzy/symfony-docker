# Packs

## ORM_PACK

When the ORM pack is enabled, a `postgres` service will be added to the
`compose.local.yaml`.

The following dependencies will also be required:

- doctrine/doctrine-bundle
- doctrine/doctrine-migrations-bundle
- doctrine/orm
- symfony/uid
- hautelook/alice-bundle _(dev only)_

A `Book` entity will already be available as an example. You can simply test the
database setup by running the following commands:

```shell
$ make migration # Create a new migration
$ make database # Re-create the database, run the migrations and load the fixtures
```

If you take a look in your database, you should see the `book` table with the
fixtures loaded in it.

## API_PACK

When the API pack is enabled, the ORM pack is automatically enabled.

The following dependencies will also be required:

- api-platform/core
- nelmio/cors-bundle
- symfony/security-bundle
- symfony/validator

The `Book` entity is also updated to be an API Resource. The API documentation
should be available at the root path: `http://localhost/` and should display the
documentation for the `/books` endpoints.

## QA_PACK

The QA pack will install the following dependencies:

- friendsofphp/php-cs-fixer _(dev only)_
- phpstan/phpstan _(dev only)_

If the ORM pack is enabled, phpstan/phpstan-doctrine will also be installed.

You can run the following commands to check the quality of your code:

```shell
$ make php-cs-fixer # Run PHP CS Fixer
$ make phpstan # Run PHPStan
$ make qa # Run both
```

## TESTING_PACK

The testing pack will install the following dependencies:

- phpunit/phpunit _(dev only)_
- behat/behat _(dev only)_
- friends-of-behat/symfony-extension _(dev only)_
- symfony/browser-kit _(dev only)_
- symfony/http-client _(dev only)_

The following dependencies may be installed if ORM and/or API packs are enabled:

- dama/doctrine-test-bundle _(dev only)_
- boulzy/behat-api-platform-bundle _(dev only)_

Follow the [Symfony documentation to write PHPUnit tests](https://symfony.com/doc/current/testing.html), or
see the examples to write Behat tests.

## DEV_PACK

The dev pack will install the following dependencies:

- symfony/var-dumper
- symfony/profiler-pack 
