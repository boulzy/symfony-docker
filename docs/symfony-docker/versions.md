# Versions

## PHP version

By default, the image `php:8.3-fpm-alpine` is used as the base image for the
application image. You can change the PHP version using the `PHP_VERSION`
argument during build, but it is recommended to update the [`Dockerfile`](./Dockerfile)
directly to ensure all images use the same version.

```Dockerfile
# ./Dockerfile

ARG PHP_VERSION="7.3"
```

If you're building a CLI application, you may also want to use the
`php:8.3-alpine` image as a base instead of the php-fpm image. In this case, you
should also remove the references to the `nginx` service in the Docker Compose
configuration files.

```Dockerfile
# Dockerfile

ARG PHP_VERSION="7.3"
FROM ${CONTAINER_PROXY}php:$PHP_VERSION-alpine AS php
```

## Symfony version

By default, the latest version of Symfony will be installed.

To install another version on Symfony, you can use the `SYMFONY_VERSION`
environment variable.

To install an unstable version of Symfony, you will need to change the minimum
stability allowed by Composer. You can do that using the `COMPOSER_STABILITY`
environment variable.

```yaml
# compose.local.yaml

services:
    app:
        environment:
            SYMFONY_VERSION: 7.1
            COMPOSER_STABILITY: beta
```
