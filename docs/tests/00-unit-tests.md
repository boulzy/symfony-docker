# Unit Tests

Unit Tests are used to test the Domain layer.
Each piece of code that enforces domain rules should be covered by Unit Tests.

Unit Tests are run using the "unit-tests" test suite from PHPUnit:

```bash
$ make unit-tests
# or in the application container:
$ vendor/bin/phpunit --testsuite unit-tests
```

See the [PHPUnit configuration](../../phpunit.xml.dist) for more details.

## Write a Unit Test

A Unit Test should implement the `PHPUnit\Framework\TestCase` class. To ensure
that tests are isolated, the dependencies required by the tested code should be
mocked.
