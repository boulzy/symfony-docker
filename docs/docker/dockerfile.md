# The Dockerfile

The Dockerfile use multi-stage builds to provide different versions of the image
suitable for different usages: local development, CI jobs and production.

## Stages

### build

The `build` stage is used as the base for all the others. It is used to install
the tools and the default configuration of the image.

### build-dev

The `build-dev` stage adds some development tools to the `build` stage, such as
[XDebug](https://xdebug.org/).

### prod

The `prod` stage is used to build a production ready version of the image.
The image is shipped with the sources and is optimized for production.

You should use environment variables to configure your application in a production
environment.

### dev

The `dev` stage is used for local development. It doesn't include the sources,
which should be mounted using a volume.

### ci

The `ci` stage is similar to the `prod` stage, but based on the `build-dev`
stage. It also installs dev dependencies to be able to run tests and CI tools.

## PHP

The image is based on the official PHP Docker image. It uses the php-fpm alpine
version.

### Update the PHP version

When you need to update the PHP version, start by updating the version using the
build argument `PHP_VERSION` in your `compose.local.yaml`:

```yaml
# compose.local.yaml
services:
    app:
        build:
            args:
                PHP_VERSION: 8.3
```

Then, rebuild the image and run your tests. Once everything is OK, you can remove
the build argument and **update the PHP version in the Dockerfile directly**.

### PHP extensions

The Dockerfile uses [mlocati/docker-php-extension-installer](https://github.com/mlocati/docker-php-extension-installer)
to easily install PHP extensions while keeping the image size optimized.
