# Local Development Environment

The local development environment provide all the services required to run the
application on your host machine.

The local development configuration is stored in `compose.local.yaml`.

## Services

### app

The `app` service is the main service of the application. When up, a php-fpm
process listen for incoming requests.

With the local environment, the host project directory is mounted in the container
to synchronize the sources in the host and the container. The `var/` directory is
excluded from the mount.

Most of the time, you will need to run commands from inside this container. You
can open a shell to it by using the following command:

```shell
$ COMPOSE_FILE=compose.yaml:compose.local.yaml docker compose exec app sh
# or, if the service is not running:
$ COMPOSE_FILE=compose.yaml:compose.local.yaml docker compose run --rm app sh
```

### nginx

The `nginx` service handle HTTP requests, using the `app` service to handle HTTP
requests. The host `public/` directory is mounted at `/var/www/html` to
synchronize the HTTP entrypoint and the public assets.

#### Accessing the Nginx service through HTTP

The `nginx` service is configured to be managed by **boulzy/traefik-docker**.
If you don't have this project running on your host, you must expose port 80
to be able to reach the service from HTTP clients, such as your browser or from
Postman.

```yaml
# compose.override.yaml
services:
    nginx:
        ports:
            - '80:80'
```

When you start the services, the application should be available at
http://localhost/.

### postgres

The `postgres` service is used as the project database for local development.

#### Accessing the database

From other containers, the database is reachable using the following DSN:

```
postgresql://docker:docker@postgres:5432/project-name?serverVersion=16.0&charset=utf8
```

If you need to access the database from outside the container, like from a UI
administration tool, you can expose the port 5432 of the service:

```yaml
# compose.override.yaml
services:
    postgres:
        ports:
            - '5435:5432'
```

And then access the database with the following DSN:

```
postgresql://docker:docker@postgres:5435/project-name?serverVersion=16.0&charset=utf8
```

Alternatively, you can setup a web-based UI administration tool, such as pgAdmin.
A working example is available in `compose.override.yaml.dist`.
