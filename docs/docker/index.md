# Docker environment

This project uses Docker as a runtime environment, both for local development,
CI jobs or deployment on production.

## The application image

At the core of the Docker environment is the application image. It is built
from the [`Dockerfile`](../../Dockerfile) at the root of the project.

Using multi-stage builds, the Dockerfile can produce images suitable for
multiple environments: local development, CI jobs, and production.

[Learn more about the Dockerfile](dockerfile.md).

## The Docker Compose setup

Docker Compose services describe the infrastructure requirements of the project.
All elements necessary to their setup will be stored in `.docker`, including
the elements required by the application image.

### Compose environments

The default Docker Compose configuration is described in `compose.yaml`.

An additional configuration file is available for each environment requiring
more configuration, named according to the pattern `compose.{{env}}.yaml`.

Currently, there is two environment-specific configuration files:

- `compose.local.yaml`
- `compose.prod.yaml`

When running Compose commands, you can specify the configuration files to use
with the `COMPOSE_FILE` environment variable:

```shell
$ COMPOSE_FILE=compose.yaml:compose.local.yaml docker compose up -d
```

> ℹ️ The CI environment uses the application image only, that's why it doesn't
> have a specific configuration file. This may change if the project requires
> more complex infrastructure, easier to provide with a Docker Compose setup.

### Host-specific configuration

If your host requires a specific configuration, you can override the Compose
configuration files in `compose.override.taml`.

This file is not committed, so your specific configuration does not impact
other environments. An example can be found in `compose.override.yaml.dist` to
enable Xdebug with an IDE.

```shell
$ COMPOSE_FILE=compose.yaml:compose.local.yaml:compose.override.yaml docker compose up -d
```

## Learn more

[Learn more about the local development environment](local.md).  
[Learn more about the production environment](prod.md).  
