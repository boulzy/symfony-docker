# Make commands

This project includes a `Makefile` to ease the developer experience.
`make` commands execute a set of instructions and are used to automate the most
common and/or painful tasks.

> ℹ️ Run `make` or `make help` to see the available commands.

_Some commands in this documentation may not be available, depending on the
project requirements._

## Configuration

`make` is by default configured to use the local development Docker Compose
environment and the `dev` Symfony environment.

The configuration is done in the `.make/.env` file. The file should have been
automatically created during the installation of the project and contain the
following:

```dotenv
# .make/.env

ENV=local
APP_ENV=dev
```

If the `ENV` variable is set, and a `compose.${ENV}.yaml` file exists, it will
be used in addition of the `compose.yaml` file to run Docker Compose commands
from the Makefile.

The `APP_ENV` variable value will be used as the default Symfony environment
used when running Symfony commands from the Makefile.

If you want to run the production environment on your host machine, run the
following command:

```shell
$ make make-init ENVS=ENV=prod APP_ENV=prod
```

This will update the `.make/.env` file to the following:

```dotenv
# .make/.env

ENV=prod
APP_ENV=prod
```

To return to the local development environment, run:

```shell
$ make make-init
```

## Commands

### Docker

#### `make build`

Build the Docker Compose services images.

**Arguments:**

`ARGS`: Arguments to pass to the Docker command.  
`SERVICE`: Specify the service to build. If not set, all services are built.

```shell
$ make build ARGS="--pull --no-cache" SERVICE=nginx
```

#### `make up`

Start the Docker Compose services.

**Arguments:**

`ARGS`: Arguments to pass to the Docker command.

```shell
$ make up ARGS="--detach --remove-orphans"
```

#### `make stop`

Stop the Docker Compose services.

```shell
$ make stop
```

#### `make down`

Stop and remove the Docker Compose services.

**Arguments:**

`ARGS`: Arguments to pass to the Docker command.

```shell
$ make down ARGS="--volumes --remove-orphans"
```

#### `make restart`

Restart the Docker Compose services.

```shell
$ make restart
```

#### `make reset`

Stop, remove, rebuild and restart the Docker Compose services.

```shell
$ make reset
```

#### `make logs`

Show the Docker Compose services logs.

**Arguments:**

`ARGS`: Arguments to pass to the Docker command.

```shell
$ make logs ARGS="--tail=0 --follow"
```

#### `make exec`

Execute a command in a Docker Compose service. Start a container if none exist.

**Arguments:**

`SERVICE`: The service in which to run the command. `app` by default.  
`ARGS`: Arguments to pass to the Docker command.
`CMD`: The command to execute.

```shell
$ make exec SERVICE=app CMD="sh"
```

#### `make sh`

Open a shell in a Docker Compose service.

**Arguments:**

`SERVICE`: The service in which to run the command. `app` by default.  
`ARGS`: Arguments to pass to the Docker command.

```shell
$ make sh SERVICE=nginx
```

### Symfony

#### `make cc`

Clear Symfony cache.

**Arguments:**

`APP_ENV`: The Symfony environment to clear.

```shell
$ make cc APP_ENV=test
```

#### `make composer`

Install the vendors.

```shell
$ make composer
```

### Database

#### `make migration`

Create a new migration.

**Arguments:**

`ARGS`: The command arguments.

```shell
$ make migration ARGS="--no-interaction"
```

#### `make migrate`

Execute the migrations.

**Arguments:**

`ARGS`: The command arguments.

```shell
$ make migrate ARGS="--no-interaction --allow-no-migration"
```

#### `make database`

Setup a fresh database.

**Arguments:**

`ARGS`: The command arguments.

```shell
$ make database ARGS="--drop --fixtures"
```

### Tests

#### `make test`

Run all the test suites.

```shell
$ make test
```

#### `make phpunit`

Run PHPUnit test suites.

```shell
$ make phpunit
```

#### `make behat`

Run Behat test suites.

```shell
$ make behat
```

### QA

#### `make qa`

Run all the quality analisys tools.

```shell
$ make qa
```

#### `make php-cs-fixer`

Run PHP CS Fixer.

```shell
$ make php-cs-fixer
```

#### `make phpstan`

Run PHPStan.

```shell
$ make phpstan
```

### Local Setup

#### `make check-requirements`

Check that your host machine fit the project requirements.

```shell
$ make check-requirements
```

#### `make install`

Install the project. You can also use this command to completely re-install the
project.

```shell
$ make install
```
